<?php

namespace common\models;

use Yii;

class ActiveRecord extends \yii\db\ActiveRecord {  
    
    protected static $id_input = 1;
    
    //Для кеширования
    const CACHE_KEY_PREFIX = 'Yii.Models.';
    public $cachingDuration = 30;
    public $cacheID = 'cache';
    
    protected function cashed($key = false) {
        $component = $this->cacheID;
        if ($this->cachingDuration > 0 && $component !== false && ($cache = Yii::$app->$component) !== null && $key) {
            if (($data = $cache->get($key)) !== false) {
                return unserialize($data);
            }
        }
        return false;
    }
    
    public static function tableName() {
        return "{{%".static::materialName()."}}";
    }
  
    public static function materialName() {
        return static::getShortName(true);
    }
    
    public static function getShortName($lower = false) {
        $name = (new \ReflectionClass(self::className()))->getShortName();
        return  ($lower)?strtolower($name):$name; 
    }
        
    public function prepare($data) {
        $class = $this::getShortName();
        foreach ($data as $key => $value) {
            if (array_key_exists($key, $this->attributes)) {
                $data[$class][$key] = $value;
            }
        }
        return $this->load($data);
    }
    
    public function loadScenario($default = false, $id_item = false) {
        $class = $this::getShortName();
        $post = Yii::$app->request->post();
        if (!$id_item)
            $this->scenario = (isset($post[$class]['scenario']))?$post[$class]['scenario']:($default?$default:"default");
        else 
            $this->scenario = (isset($post[$class][$id_item]['scenario']))?$post[$class][$id_item]['scenario']:($default?$default:"default");
    }
    
    public function prepareForSearch($data, $conformity = []) {
        $result = [];
        $class = $this::getShortName();
        foreach ($data as $key => $value) {
            //Если в запросе поле отличное от поля в базе, то учитывать это
            $field = isset($conformity[$key])?$conformity[$key]:$key; 
            if (array_key_exists($field, $this->attributes)) {
                $result[$field] = $value;
            }
        }
        return $result;
    }
    
    public function selectedFields($select = [], $language = "ru") {
        $result = [];
        if (is_array($select)) {
            $fields = $this->fieldsList($language);
            foreach (array_values($select) as $field) {
                if (in_array($field, $fields))
                    $result[] = $field;
            }
        }
        else $result = $select;
        return $result;
    }
    
    public function fieldsList($language = "ru") {
        $fields = [];
        foreach ($this->attributes as $key => $value) {
            $matche = null;
            if (preg_match('/\\w+_(ru|en)$/', $key, $matche)) {
                if ($matche[1] == $language)
                    $fields[] = $key;
                else continue;
            }
            else $fields[] = $key;
                
            
        }
        return $fields;
    }
    
    public function fieldsValue($language = "ru", $select = "*") {
        $fields = []; $rename = []; $no = [];
        if (is_array($select)) {
            while ($item = current($select)) {
                $key = key($select);
                if (!is_int($key)) {
                    $rename[$item] = $key;
                }
                if (!isset($this->attributes[$item]))
                    $no[] = $item;
                next($select);
            }
        }
        foreach ($this->attributes as $key => $value) {
            $matche = null;
            if (preg_match('/\\w+_(ru|en)$/', $key, $matche)) {
                if ($matche[1] == $language)
                    $fields[$key] = $value;
                else continue;
            }
            //Если поле входит в массив выбираемых
            else if ((is_array($select) && in_array($key, $select)) || !is_array($select)) {
                if (isset($rename[$key]))
                    $fields[$rename[$key]] = $value;
                else $fields[$key] = $value;
            }
            else continue;
        }
        foreach ($no as $field) 
            if (isset($this->$field))
                $fields[$field] = $this->$field;
        
        //Yii::$app->debug->show($fields);
        return $fields;
    }
    
    public function fieldsClear() {
        foreach ($this->attributes as $key => $value) {
            if (is_string($this->attributes[$key])) {
                if (!in_array($key, ["text"])) {
                    $this->$key = strip_tags(trim($value));
                    
                }
                else $this->$key = trim($value);
                $this->$key = str_replace('"', "'", $this->$key);
                $this->$key = str_replace('&quot;', "'", $this->$key);
                
                //$this->$key = str_replace('"', "&quot;", $this->$key);
            }
        }
        return $this;
    }
    
    public function checkSimpleQuery($params = [], $field = null) {
        $simple = ["limit", "page", "language"];
        array_push($simple, $field);
        foreach ($params as $p => $v) 
            if (!in_array($p, $simple)) 
                return false;
        return true;
    }
    
    public static function getIdForInput() {
        return "wd".self::$id_input++;
    }
    
    //Модифицирует массив $_FILES согласно текущей м
    public function sortFilesVariable($files = [], $id = false, $reset = true) {
        $class = $this->getShortName();
        $_files = [];
        if ($reset)
            \yii\web\UploadedFile::reset();
        if (isset($files[$class])) {
            foreach ($files[$class] as $key => $var) {
                if (isset($var[$id]))
                    $_files[$class][$key] = $files[$class][$key][$id];
            }
        }
        $_FILES = $_files;
    }
    
    public static function loadModels($models = false, $post = false) {
        if (!$post)
            $post = Yii::$app->request->post();
        if ($models) {
            $result = false;
            foreach ($models as &$model) {
                if (isset($post[$model->getShortName()][$model->id])) {
                    $result = true;
                    $model->prepare($post[$model->getShortName()][$model->id]);
                }
            }
            if ($result)
                return $models;
        }
        return false;
    }
    
    public function isDefaultScenario() {
        return ($this->scenario == "default")?true:false;
    }
}

