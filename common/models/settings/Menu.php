<?php

namespace common\models\settings;

use Yii;

/**
 * This is the model class for table "settings_menu".
 *
 * @property integer $id
 * @property integer $id_language
 * @property integer $id_main
 * @property string $title
 * @property string $url
 * @property string $icon
 * @property integer $position
 * @property integer $in_new
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 * @property string $seo_other
 */
class Menu extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'settings_menu';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_language', 'id_main', 'position', 'in_new'], 'integer'],
            [['title', 'url'], 'required'],
            [['title'], 'string', 'max' => 100],
            [['url', 'icon', 'seo_title', 'seo_keywords', 'seo_description', 'seo_other'], 'string', 'max' => 255],
            [['position', 'in_new'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'id_language' => Yii::t('settings', 'Id Language'),
            'id_main' => Yii::t('settings', 'Id Main'),
            'title' => Yii::t('settings', 'Title'),
            'url' => Yii::t('settings', 'Url'),
            'icon' => Yii::t('settings', 'Icon'),
            'position' => Yii::t('settings', 'Position'),
            'in_new' => Yii::t('settings', 'In New'),
            'seo_title' => Yii::t('material', 'Seo Title'),
            'seo_keywords' => Yii::t('material', 'Seo Keywords'),
            'seo_description' => Yii::t('material', 'Seo Description'),
            'seo_other' => Yii::t('material', 'Seo Other'),
        ];
    }
    
    public static function getList($language = "ru") {
        if ($id_language = \app\models\Language::getLanguageId($language))
            return SettingsMenu::find()->where(['id_language' => $id_language])->orderBy('position ASC')->all();
        else return array();
    }

}
