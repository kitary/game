<?php
namespace common\models\material;

use Yii;
use yii\web\UploadedFile;

use common\models\ActiveRecord;
use common\components\UploadImageBehavior;

/**
 * This is the model class for table "news_images".
 *
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_material
 * @property string $title
 * @property string $file
 * @property string $url
 * @property string $description
 * @property integer $size
 * @property string $ext
 * @property integer $position
 * @property string $date_create
 * @property string $date_update
 */
class Images extends ActiveRecord {
    
    public $_temp_images;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_material', 'date_create'], 'required'],
            [['id_user', 'id_material', 'position', 'size', 'is_active', 'is_delete'], 'integer'],
            [['date_create', 'date_update'], 'safe'],
            [['title', 'url'], 'string', 'max' => 255],
            [['_temp_images'], 'file', 'extensions' => 'jpg, jpeg, gif, png', 'maxFiles' => 20],
            ['file', 'file', 'extensions' => 'jpg, jpeg, gif, png', 'on' => ['insert', 'update']],
            [['description'], 'string', 'max' => 1000],
            [['ext'], 'string', 'max' => 10],
            [['is_active', 'is_delete'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'id_material' => Yii::t('material', 'Id Material'),
            'title' => Yii::t('material', 'Title'),
            'file' => Yii::t('material', 'File'),
            'url' => Yii::t('material', 'Url'),
            'description' => Yii::t('material', 'Description'),
            'ext' => Yii::t('material', 'Ext'),
            'position' => Yii::t('material', 'Position'),
            'date_create' => Yii::t('material', 'Date Create'),
            'date_update' => Yii::t('material', 'Date Update'),
            'is_active' => Yii::t('material', 'Main image'),
        ];
    }
    
    function behaviors() {
        return [
            [
                'class' => UploadImageBehavior::className(),
                'attribute' => 'file',
                'scenarios' => ['insert', 'update'],
                'placeholder' => '@webroot/upload/default.jpg',
                'path' => '@webroot/upload/'.Settings::getInstance()->getModuleId().'/{id_material}',
                'url' => '@web/upload/'.Settings::getInstance()->getModuleId().'/{id_material}',
                'thumbPath' => '@webroot/upload/'.Settings::getInstance()->getModuleId().'/{id_material}/thumb',
                'thumbUrl' => '@web/upload/'.Settings::getInstance()->getModuleId().'/{id_material}/thumb',
                'thumbs' => [
                    'thumb' => ['width' => 940, 'height' => 640, 'quality' => 90],
                    'preview' => ['width' => 94, 'height' => 64],
                ],
            ],
        ];
    }    
    
    public function getMaterial() {
        $class = Settings::getInstance()->getModulePath()."\models\Material";
        return $this->hasOne($class::className(), ['id' => 'id_material']);
    }
    
    public static function addImage($id = false, $image = false) {
        if ($image instanceof UploadedFile) {
            $class = Settings::getInstance()->getModulePath()."\models\MaterialImages";
            $model = new $class;
            if (!Yii::$app->user->isGuest)
                $model->id_user = Yii::$app->user->id;
            $model->scenario = "insert";
            $model->id_material = $id;
            $model->date_create = date("Y-m-d G:i:s", time());
            $model->size = $image->size;
            $model->file = $image;
            $type= preg_split('/\s*\/\s*/', $image->type, -1, PREG_SPLIT_NO_EMPTY);
            $model->ext = isset($type[1])?$type[1]:NULL;
            if ($model->save()) {
                return $model->id;
            }
        }
    }
    
//    public static function eraseImagesFromObject($id = false) {
//        $items = static::updateAll(["is_active" => 0], ['id_material' => $id]);
//        foreach ($items as $item) {
//            $item->delete();
//        }
//    }
    
    public static function deleteImagesFromObject($id = false) {
        $items = static::find()->where(['id_material' => $id])->all();
        foreach ($items as $item) {
            $item->delete();
        }
    }

}
