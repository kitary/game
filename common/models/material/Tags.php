<?php

namespace common\models\material;

use Yii;
use yii\data\ActiveDataProvider;

use app\models\ActiveRecord;
use app\components\Settings;

/**
 * This is the model class for table "material_tags".
 *
 * @property integer $id
 * @property string $material
 * @property string $name
 * @property integer $frequency
 */
class Tags extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material_tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['frequency'], 'integer'],
            [['material'], 'string', 'max' => 100],
            [['name'], 'string', 'max' => 128],
            [['name', 'material'], 'unique', 'targetAttribute' => ['name'], 'on' => ['insert'], 'message' => Yii::t('material', 'The combination of tag and material has already been taken.')],
            [['seo_title', 'seo_description', 'seo_keywords', 'seo_site', 'seo_other'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'material' => 'Material',
            'name' => 'Name',
            'frequency' => 'Frequency',
            'seo_title' => Yii::t("material", 'Seo Title'),
            'seo_description' => Yii::t("material", 'Seo Description'),
            'seo_keywords' => Yii::t("material", 'Seo Keywords'),
            'seo_site' => Yii::t("material", 'Seo Site'),
            'seo_other' => Yii::t("material", 'Seo Other'),
        ];
    }
    
    /**
     * Returns tag names and their corresponding weights.
     * Only the tags with the top weights will be returned.
     * @param integer the maximum number of tags that should be returned
     * @return array weights indexed by tag names.
     */
    public function findTagWeights($limit = 20, $type = false) {
        $tags = false;
        $component = $this->cacheID;
        if (($key = self::CACHE_KEY_PREFIX . Settings::getInstance()->getModuleId().".".$type."_".$limit) && !($tags = $this->cashed($key))) {
            
            $items = $this->find();
            //if ($type)
            $items->andWhere(['material' => ($type)?$type:["news", "events", "articles"]]);
            $models = $items->orderBy('frequency DESC, `name` ASC')->limit($limit)->all();

            //Вычисляем список тегов для заданных условий
            $total = 0;
            foreach ($models as $model)
                $total+=$model->frequency;

            $tags = [];
            if ($total > 0) {
                foreach ($models as $model)
                    $tags[$model->name] = 8 + (int) (16 * $model->frequency / ($total + 10));
                ksort($tags);
            }
            
            if (($cache = Yii::$app->$component) !== null) {
                $cache->set($key, serialize($tags), $this->cachingDuration);
            }
        } 
        
        return $tags;
    }

    /**
     * Suggests a list of existing tags matching the specified keyword.
     * @param string the keyword to be matched
     * @param integer maximum number of tags to be returned
     * @return array list of matching tag names
     */
    public function suggestTags($keyword, $limit = 20) {
        $tags = $this->findAll([
            'condition' => 'name LIKE :keyword',
            'order' => 'frequency DESC, Name',
            'limit' => $limit,
            'params' => array(
                ':keyword' => '%' . strtr($keyword, array('%' => '\%', '_' => '\_', '\\' => '\\\\')) . '%',
            ),
        ]);
        $names = array();
        foreach ($tags as $tag)
            $names[] = $tag->name;
        return $names;
    }

    public static function string2array($tags) {
        return preg_split('/\s*,\s*/', trim($tags), -1, PREG_SPLIT_NO_EMPTY);
    }

    public static function array2string($tags) {
        return implode(' ', $tags);
    }

    public function updateFrequency($material = false, $oldTags, $newTags) {
        $oldTags = self::string2array($oldTags);
        $newTags = self::string2array($newTags);
        
        $this->addTags($material, array_values(array_diff($newTags, $oldTags)));
        $this->removeTags($material, array_values(array_diff($oldTags, $newTags)));
    }

    public function addTags($material, $tags) {
        //Yii::$app->debug->show($tags);
        $this->updateAllCounters(['frequency' => 1], ["and", ["in", "name", $tags], ["material" => $material]]);
        foreach ($tags as $name) {
            if (!Tags::find()->where(['and', ['name' => trim($name)], ["material" => $material]])->count()) {
                $tag = new Tags;
                $tag->material = $material;
                $tag->name = trim($name);
                $tag->frequency = 1;
                if ($tag->validate())
                    $tag->save();
                //else Yii::$app->debug->show($tag->getErrors()); 
                   
            }
        }
    }

    public function removeTags($material = false, $tags) {
        if (empty($tags))
            return;
        $this->updateAllCounters(['frequency' => -1], ["and", ["in", "name", $tags], ["material" => $material]]);
        $this->deleteAll("frequency <= 0 AND material='{$material}'");
    }
    
    public static function getTagName($id = false) {
        $tag = static::findOne(['id' => $id]);
        return ($tag)?$tag->getAttribute('name'):false;
    }
    
    public function search($params, $material = false) {
        $query = ($material)?static::find()->where(['material' => $material]):static::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 50],
        ]);
        
        $dataProvider->sort = ['defaultOrder' => ['id'=>SORT_DESC]];

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['frequency' => $this->frequency]);
        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'material', $this->material]);

        return $dataProvider;
    }
}
