<?php

namespace common\models\material;

use Yii;
use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii2mod\bxslider\BxSlider;

use common\models\User;
use common\models\ActiveRecord;
use common\models\local\Language;

//use common\components\Site;
use common\components\Settings;
use common\components\UploadBehavior;
use common\components\UploadImageBehavior;

use common\models\material\Tags;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property integer $id_main
 * @property integer $id_area
 * @property integer $id_city
 * @property integer $id_user
 * @property integer $id_language
 * @property string $title
 * @property string $slug
 * @property string $short_text
 * @property string $text
 * @property string $image_file
 * @property string $image
 * @property string $video
 * @property string $date_created
 * @property string $date_update
 * @property string $date_public
 * @property string $site
 * @property string $seo_title
 * @property string $seo_description
 * @property string $seo_keywords
 * @property integer $view_count
 * @property integer $comments_count
 * @property double  $rating
 * @property integer $rating_voting_count
 * @property integer $is_show
 * @property integer $is_show_in_top
 * @property integer $is_selected
 * @property integer $gallery
 * @property integer $delivery_date
 * @property string $comment
 */
class Material extends ActiveRecord {
    
    public $_type = null;

    public $_section;
    public $_sub_section;
    public $username;
    
    public $_oldTags;
    public $_temp_gallery;
    
    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;
    const STATUS_ARCHIVED = 2;
    const STATUS_BANNED = 3;
    const STATUS_DELETED = 4;
    
    const TEMPLATE_DEFAULT = 0;
    const TEMPLATE_WITHOUT_WIDGETS = 1;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_area', 'id_main', 'id_city', 'id_user', 'id_language', 'id_section', 'id_subsection', 'id_icon', 'view_count', 'comments_count', 'rating_voting_count', 'is_show', 'is_show_in_top', 'is_show_on_main', 'is_selected', 'gallery', 'order'], 'integer'],
            [[/*'id_section', 'id_subsection',*/ 'id_user', 'id_language', 'title', 'text', 'date_created', 'date_public', 'slug'], 'required'],
            [['short_text', 'text', 'tags', 'slug', 'js'], 'string'],
            ['rating', 'number'],
            ['rating', 'match', 'pattern'=>'/^[0-9]{1,2}(\.[0-9])?$/', 'message' => Yii::t("material", "Out of range")],
            ['image_file', 'file', 'extensions' => 'jpg, jpeg, gif, png', 'on' => ['insert', 'update']/* , 'maxFiles' => 5 */],
            ['_temp_gallery', 'file', 'extensions' => 'jpg, jpeg, gif, png', 'maxFiles' => 20],
            [['_section', '_sub_section', 'username', 'date_created', 'date_update', 'date_public', 'delivery_date'], 'safe'],
            [['image', 'video', 'site', 'hash_tag', 'slug'], 'string', 'max' => 255],
            [['title'], 'string', 'max' => 1000],
            ['image', 'url'],
            [['slug'], 'unique'],
            [['seo_title', 'seo_description', 'seo_keywords', 'comment'], 'string', 'max' => 512],
            [['view_count', 'comments_count', 'rating', 'is_show', 'is_show_on_main', 'is_show_in_top', 'is_selected', 'gallery', 'rating_voting_count'], 'default', 'value' => 0],
            ['is_show', 'in', 'range' => [self::STATUS_DRAFT, self::STATUS_PUBLISHED, self::STATUS_ARCHIVED, self::STATUS_BANNED, self::STATUS_DELETED], 'message' => 'Invalide status'],
            ['template', 'in', 'range' => [self::TEMPLATE_DEFAULT, self::TEMPLATE_WITHOUT_WIDGETS], 'message' => 'Invalide template'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'id_area' => Yii::t("material", 'Country'),
            'id_city' => Yii::t("material", 'City'),
            'id_user' => Yii::t("material", 'User'),
            'id_language' => Yii::t("material", 'Language'),
            'title' => Yii::t("material", 'Title'),
            'id_section' => Yii::t("material", 'Type'),
            'id_subsection' => Yii::t("material", 'Sub Type'),
            'short_text' => Yii::t("material", 'Short Text'),
            'text' => Yii::t("material", 'Text'),
            'image_file' => Yii::t("material", 'Image'),
            'image' => Yii::t("material", 'Image link'),
            'video' => Yii::t("material", 'Video'),
            'js' => Yii::t("material", 'Java Script'),
            'date_created' => Yii::t("material", 'Date Created'),
            'date_update' => Yii::t("material", 'Date Update'),
            'date_public' => Yii::t("material", 'Public date'),
            'site' => Yii::t("material", 'Site'),
            'seo_title' => Yii::t("material", 'Seo Title'),
            'seo_description' => Yii::t("material", 'Seo Description'),
            'seo_keywords' => Yii::t("material", 'Seo Keywords'),
            'view_count' => Yii::t("material", 'View Count'),
            'is_show' => Yii::t("material", 'Status'),
            'is_show_on_main' => Yii::t("material", 'Show on main page'),
            'is_show_in_top' => Yii::t("material", 'Show in top'),
            'is_selected' => Yii::t("material", 'Selected'),
            'gallery' => Yii::t("material", 'Gallery'),
            'comment' => Yii::t("material", 'Comment'),
            '_section' => Yii::t("material", 'Section'),
            '_sub_section' => Yii::t("material", 'Sub-Section'),
            'delivery_date' => Yii::t("material", 'Delivery'),
            'hash_tag' => Yii::t("material", 'Instagram tags'),
            'tags' => Yii::t("material", 'Tags'),
            '_temp_gallery' => Yii::t("material", 'Add images to the gallery'),
            'slug' => Yii::t("material", 'Path'),
            'id_icon' => Yii::t("material", 'Icon'),
            'template' => Yii::t('material', "Template"),
        ];
    }
    
    public static function getStatusLabels() {
        return [
            self::STATUS_DRAFT => Yii::t("material", "Draft"),
            self::STATUS_PUBLISHED => Yii::t("material", "Published"),
            self::STATUS_ARCHIVED => Yii::t("material", "Archived"),
            self::STATUS_BANNED => Yii::t("material", "Banned"),
            self::STATUS_DELETED => Yii::t("material", "Deleted"),
        ];
    }
    
    public static function getTemplatesLabels() {
        return [
            self::TEMPLATE_DEFAULT => Yii::t("material", "Default"),
            self::TEMPLATE_WITHOUT_WIDGETS => Yii::t("material", "Without widgets"),
        ];
    }

    function behaviors() {
        return [
            [
                'class' => UploadImageBehavior::className(),
                'attribute' => 'image_file',
                'scenarios' => ['insert', 'update'],
                //'unlinkOnSave' => false, 
                'placeholder' => '@webroot/upload/default.jpg',
                'path' => '@webroot/upload/'.Settings::getInstance()->getModuleId().'/{id}',
                'url' => '@web/upload/'.Settings::getInstance()->getModuleId().'/{id}',
                'thumbPath' => '@webroot/upload/'.Settings::getInstance()->getModuleId().'/{id}/thumb',
                'thumbUrl' => '@web/upload/'.Settings::getInstance()->getModuleId().'/{id}/thumb',
                'thumbs' => [
                    'thumb' => ['width' => 400, 'quality' => 90],
                    'preview' => ['width' => 200, 'height' => 200],
                ],
            ],
            'sortable' => [
                'class' => \kotchuprik\sortable\behaviors\Sortable::className(),
                'query' => self::find(),
            ],
            'slug' => [
                    'class' => 'app\modules\material\components\behaviors\Slug',
                    'in_attribute' => 'title',
                    'out_attribute' => 'slug',
                    'translit' => true
            ]
        ];
    }
    
    public function isShowDelivery() {
        return in_array(Settings::getInstance()->getModuleId(), ['news', 'events', 'articles', 'company']);
    }

    public function afterFind() {
        parent::afterFind();
        $this->_oldTags = $this->tags;
        $host = (Yii::$app instanceof \yii\console\Application)?Yii::getAlias('@web'):Yii::$app->request->hostinfo;
        if (!empty($this->image_file) && !is_null($this->image_file)) {
            //if (empty($this->image))
            $this->image = $host . $this->getUploadUrl('image_file');
            $this->image_file = $host . $this->getThumbUploadUrl('image_file', 'preview');
        }
        else if (empty($this->image) && !in_array(Settings::getInstance()->getModuleId(), ['blogs'])) {
            $this->image = $host . "/css/images/without-photo.jpg";
            $this->image_file = $host . $this->image;
        }
    }
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        $model = new Tags;
        $model->updateFrequency(Settings::getInstance()->getModuleId(), $this->_oldTags, $this->tags);
    }
    
    public function addNoFollow() {
        $matches = array();
        if (preg_match_all("/<a\s[^>]*href=([\"\']??)([^\" >]*?)\\1[^>]*>(.*)<\/a>/siU", $this->text, $matches)) {
            foreach ($matches[0] as $index => $link) {
                if (!strpos($matches[3][$index], $_SERVER['SERVER_NAME']))
                    $this->text = str_replace($matches[0][$index], Html::a($matches[3][$index], $matches[2][$index], ["rel" => "nofollow"]), $this->text);
            }
            
        }
    }
    
    /**
     * @return array a list of links that point to the post list filtered by every tag of this post
     */
    public function getTagLinks($url = false) {
        $links = [];
        $type = Settings::getInstance()->getModuleId();
        if ($tags = Tags::string2array($this->tags)) {
            foreach ($tags as $tag)
                $links[] = Html::a(Html::encode ($tag), [($url)?$url:'/admin/'.$type.'/tags/view', 'tag' => $tag, "material" => $type]);
        }
        return $links;
    }

    /**
     * Normalizes the user-entered tags.
     */
    public function normalizeTags($attribute, $params) {
        $this->tags = Tags::array2string(array_unique(Tags::string2array($this->tags)));
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage() {
        return $this->hasOne(Language::className(), ['id' => 'id_language']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
    
    public function getGalleryimages() {
        $class = Settings::getInstance()->getModulePath()."\models\MaterialImages";
        return $this->hasMany($class::className(), ['id_material' => 'id']);
    }
    
    public function clearGallery() {
        $class = Settings::getInstance()->getModulePath()."\models\MaterialImages";
        $images = $class::find()->where(["id_material" => $this->id_main])->all();
        foreach ($images as $image) {
            $image->delete();
        }
    }
    
    public function getPath($material) {
        return Site::buildPath(@$this->language->type, $material, @$this->city->slug, @$this->section->slug, @$this->subsection->slug, $this->slug);
    }
    
    public function getCityPath($material) {
        return isset($this->city->name)?Html::a(@$this->city->name, "/".Site::getInstance()->getLanguage()."/".$material."/".@$this->city->slug):false;
    }

    public function getCity() {
        return $this->hasOne(\app\models\WorldCities::className(), ['id' => 'id_city']);
    }

    public function getSection() {
        $class = Settings::getInstance()->getModulePath()."\models\Section";
        return $this->hasOne($class::className(), ['id' => 'id_section']);
    }

    public function getSubsection() {
        $class = Settings::getInstance()->getModulePath()."\models\SubSection";
        return $this->hasOne($class::className(), ['id' => 'id_subsection']);
    }
    
    public function sectionName() {
        return ($section = $this->section)?$section->name:false;
    }
    
    public function subSectionName() {
        return ($section = $this->subsection)?$section->name:false;
    }
    
    
    public static function updateGalleryStatus($id = false, $status = false) {
        static::updateAll(["gallery" => ($status?1:0)], ["id_main" => $id]);
    }

    public static function getLanguagesList($id = false) {
        $languages = [];
        $news = static::find()->where(["id_main" => $id])->all();
        foreach ($news as $new)
            $languages[$new->id_language] = Language::getTypeOfLanguage($new->id_language);
        return $languages;
    }

    public static function getMaterialsList($id = false) {
        return static::find()->where(["id_main" => $id])->orderBy("id_language ASC")->asArray()->all();
    }
    
    public static function getStatusItems() {
        $list = array();
        $class = Settings::getInstance()->getModulePath()."\models\Material";
        $object = new $class;
        foreach ($object->getValidators('is_show') as $validator) {
            if ($validator instanceof \yii\validators\RangeValidator && $validator->range !== null) {
                foreach($validator->range as $key => $value) 
                    $list[$value] = static::getStatusName($value);
                break;
            }
        }
        return $list;
    }
    
    public static function getStatusName($status = false) {
        $list = static::getStatusLabels();
        return (isset($list[$status]))?$list[$status]:"";
    }
    
    public static function getTemplatesItems() {
        $list = array();
        $class = Settings::getInstance()->getModulePath()."\models\Material";
        $object = new $class;
        foreach ($object->getValidators('template') as $validator) {
            if ($validator instanceof \yii\validators\RangeValidator && $validator->range !== null) {
                foreach($validator->range as $key => $value) {
                    if ($label = static::getTemplateName($value))
                        $list[$value] = $label;
                }
                break;
            }
        }
        return $list;
    }
    
    public static function getTemplateName($name = false) {
        $list = static::getTemplatesLabels();
        return (isset($list[$name]))?$list[$name]:"";
    }

    public function search($params, $id = false) {
        $class = Settings::getInstance()->getModulePath()."\models\Material";
        $query = ($id) ? $class::find()->where(["id_user" => $id]) : $class::find();
        $query->from(["material" => Settings::getInstance()->getModuleId()]);
        $query->select("section.name, subsection.name, material.*");
        $query->andWhere('material.`id_main` = material.`id`'); //Выборка только основных новостей
        $query->joinWith([
            'section' => function($query) {
                $query->from(['section' => Settings::getInstance()->getModuleId().'_section']);
            },
            'subsection' => function($query) {
                $query->from(['subsection' => Settings::getInstance()->getModuleId().'_subsection']);
            },
            'user']
        );
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort = ['defaultOrder' => ['date_public'=>SORT_DESC, "order" => SORT_ASC]];
        $dataProvider->sort->attributes['_section'] = [
            'asc' => ['section.name' => SORT_ASC],
            'desc' => ['section.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['_sub_section'] = [
            'asc' => ['subsection.name' => SORT_ASC],
            'desc' => ['subsection.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['username'] = [
            'asc' => ['user.email' => SORT_ASC],
            'desc' => ['user.email' => SORT_DESC],
        ];

        //print_r($query->createCommand()->getRawSql()); die();
        

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
            //Yii::$app->debug->show($this->getErrors());
        }
        
        $query->andFilterWhere(['id_user' => $this->id_user]);
        $query->andFilterWhere(['material.id_language' => $this->id_language]);
        $query->andFilterWhere(['like', 'title', $this->title]);
        $query->andFilterWhere(['like', 'section.name', $this->_section]);
        $query->andFilterWhere(['like', 'subsection.name', $this->_sub_section]);
        $query->andFilterWhere(['like', 'material.date_created', $this->date_created]);
        $query->andFilterWhere(['like', 'material.date_public', $this->date_public]);
        $query->andFilterWhere(['like', 'material.delivery_date', $this->delivery_date]);
        $query->andFilterWhere(['is_show' => $this->is_show]);

        return $dataProvider;
    }

    //Выдает новость в виде массива со списком комментариев к ней
    public static function getMaterialWithComments($id = false, $language = "ru", $select = "*") {
        $material = static::findOne(["id" => $id]);
        if ($material) {
            $class = Settings::getInstance()->getModulePath()."\models\Comment";
            $result = $material->fieldsValue($language, $select);
            $count = $class::find()->where(["id_material" => $id])->count();
            $result['comments'] = ["comment_ru" => "Комментарии", "total_count" => $count];
            $result['comments']['items'] = $class::getSortedCommentsList($id);
            $result['type'] = Settings::getInstance()->getModuleId();
            return $result;
        } else
            return false;
    }
    
    public function getGalleryImagesList() {
        $host = (Yii::$app instanceof \yii\console\Application)?Yii::getAlias('@web'):Yii::$app->request->hostinfo;
        $class = Settings::getInstance()->getClassName(false, Settings::IMAGE);
        $result = [];
        if ($images = $class::find()->where(['id_material' => $this->id, "is_delete" => 0])->orderBy("position ASC")->all()) {
            foreach ($images as $image) {
                $result[] = $host.$image->getThumbUploadUrl('file', 'thumb');
            }
        }
        return $result;
    }
    
    public function buildSlider($max_slides = 7, $set_active = true, $is_update = false) {
        $gallery = false;
        $class = Settings::getInstance()->getModulePath()."\models\MaterialImages";
        //Yii::$app->debug->show($class);
        $images = $class::find()->where(['id_material' => $this->id, "is_delete" => 0])->orderBy("position ASC")->all();
        $type = Settings::getInstance()->getModuleId();
        if ($images) {
            $images_items = [];
            foreach ($images as $image)
                $images_items[] = "<div class=\"slider_item\" rel=\"{$image->id}\">".Html::img($image->getThumbUploadUrl('file', 'preview'), [
                    'class' => ($image->is_active)?'img-thumbnail active':'img-thumbnail',
                    'onclick' => ($set_active)?"setActiveSlide(this, '{$type}', {$this->id}, {$image->id})":false,
                    'rel' => $image->id
                ]).(($set_active)?Html::tag("span", null, [
                    'class' => "glyphicon glyphicon-trash", 
                    "onclick" => ($set_active)?"deleteSlide(this, {$image->id}, '".Yii::t('common', 'Are you sure you want to delete this items?')."')":false,
                ]):"")."</div>";
                    
            $gallery = BxSlider::widget([
                'containerOptions' => [
                    //'style' => "margin-bottom: 5px;"
                ],
                'pluginOptions' => [
                    'minSlides' => 1,
                    'maxSlides' => (count($images_items) > $max_slides)?$max_slides:false,
                    'controls' => (count($images_items) > $max_slides)?true:false,
                    // set video property to true, if in $items array exist videos
                    'video' => false,
                    'slideWidth' => 94,
                    'pager' => false,
                    // usage events
                    'onSliderLoad' => new \yii\web\JsExpression('
                        function() {
                            //alert("Slider load");
                        },
                    ')
                ],
                'items' => $images_items 
            ]); 
        }
        return $gallery;
    }
    
    public static function fullSearch($type = false, $text = false, $is_tag = false, $limit = 10) {
        Settings::getInstance()->setModuleId($type);
        $class = Settings::getInstance()->getModulePath()."\\models\Material";
        $items = [];   
        if (class_exists("\yii\sphinx\Query")) {
            $query = new \yii\sphinx\Query;
            $index = $is_tag?$type."_tag":$type;
            $rows = $query->from($index)
                ->match($text)
                ->limit($limit)
                ->all();
            $ids = [];
            foreach ($rows as $row) 
                $ids[] = $row['id'];

            
            $materials = $class::findAll(["id" => $ids]);
            foreach ($materials as $material) 
                $items[] = $material;
            
        }
        else {
            $materials = $class::find()
                ->where(["like", "title", htmlspecialchars($text)])
                ->limit($limit)
                ->all();
            foreach ($materials as $material) 
                $items[] = $material;
        }
        return $items;
    }
    
    public static function getItems(Loader &$loader = null) {
        $class = $loader->getClass();
        $date = date("Y-m-d G:i:s", time());
        // Создать зависимость от времени модификации материала
        $dependency = new \yii\caching\DbDependency();
        $dependency->sql = 'SELECT MAX(date_update) FROM '.$class::tableName(). " WHERE `id_language` = ".intval($loader->language);
        if (!$loader->is_view && !$loader->is_main)
            $dependency->sql .= " AND `is_show_on_main` = 0";
        //Yii::$app->debug->show($dependency);
        $items = $class::getDb()->cache(function ($db) use ($class, $date, $loader){
            $it = $class::find()->where(["and", ["<=", "date_public", $date], ["is_show" => $class::STATUS_PUBLISHED]]);
            if (!$loader->is_view)
                $it->andWhere(["is_show_on_main" => ($loader->is_main?1:0)]);
            if ($loader->language)
                $it->andWhere(["id_language" => intval($loader->language)]);
            if ($loader->city)
                $it->andWhere(["id_city" => intval($loader->city)]);
            if ($loader->section)
                $it->andWhere(["id_section" => intval($loader->section)]);
            if ($loader->sub_section)
                $it->andWhere(["id_subsection" => intval($loader->sub_section)]);
            
            $page = $loader->page - 2;
            if ($page < 0) $page = 0;
            $offset = $page * $loader->limit;
            //if ($loader->page > 1) 
            //    $offset += 1;
            if ($loader->start > 0 && $loader->page > 1)
                $offset += $loader->start;
            $it->orderBy(["`date_public` DESC" => "", "`is_show_in_top` ASC" => "", "`order` ASC" => ""])
                ->offset($offset)
                ->limit($loader->limit);
            $loader->setTotal($it->count());
            return ($loader->success)?$it->all():Yii::$app->debug->show($it->createCommand()->getRawSql());
        }, $loader->getTimeout(), $dependency);
        
        return $items;
    }
    
    public function incrementViews() {
        $this->scenario = "update";
        $this->view_count++;
        if ($this->save()) {
            return $this->view_count;
        }
        else return false;
    }
    
//    V – количество голосов за фильм
//    M – порог голосов, необходимый для участия в рейтинге Топ-250 (сейчас: 100)
//    R – среднее арифметическое всех голосов за фильм
//    С – среднее значение рейтинга всех фильмов (сейчас: 7.3837)
    
//    select @a:=POW(max(count_votes), 1/10) from films;
//    select id,name,raiting, count_votes, ((LOG(@a,count_votes))+raiting)/2 as actual_raiting from films order by actual_raiting desc ;
    
//    http://habrahabr.ru/post/172065/
    public function changeRating($value = false) {
        $this->scenario = "update";
        $type = Settings::getInstance()->getModuleId();
        $class = Settings::getInstance()->getModulePath()."\models\Material";
        if ($value >= 0 && $value <= 5) {
            if ($this->rating_voting_count) {
//                $count = $class::find()->where(["is_show" => self::STATUS_PUBLISHED])->count();
//                $middle = $class::find()->select('sum(rating) as rating')->where(["is_show" => self::STATUS_PUBLISHED])->asArray()->one();
//                $m = 50;
//                $c = $middle['rating'] / $count;
//                $r = $this->rating;
//                $v = $this->rating_voting_count;
//                $rating = $v / ($v + $m) * $r + $m / ($v + $m) * $c;
//                $this->rating = round($rating, 1);
                $max = $class::find()->select('max(rating_voting_count) as count')->where(["is_show" => self::STATUS_PUBLISHED])->asArray()->one();
                $a = pow($max['count'], 1/10.0);
                $rating = ((log10($this->rating_voting_count) / log10($a)) + $this->rating) / 4;
                $this->rating = round($rating, 1);
            }
            else $this->rating = $value;
        }
        
        if ($rating = @Yii::$app->session['rating']) {
            if (!isset($rating[$type][$this->id])) {
                $this->rating_voting_count++;
            }
        } else $rating = [];
        
        $this->rating_voting_count++;
        
        if ($this->save()) {
            $id = $this->id;
            //$rating[$type][$id] = date("Y-m-d G:i:s", time());
            //Yii::$app->session['rating'] = $rating;
            return ["count" => $this->rating_voting_count, "value" => $this->rating];
        }
        else Yii::$app->debug->show($this->getErrors());// return false;
    }
    
    public function updateCommentsCount() {
        $this->scenario = "update";
        $class = Settings::getInstance()->getModulePath()."\models\Comment";
        $count = $class::find()->where(["id_material" => $this->id])->count();
        $this->comments_count = $count;
        if ($this->save()) {
            return $this->comments_count;
        }
        else return false;
    }
    
    public function isShow($materials = []) {
        $type = Settings::getInstance()->getModuleId();
        if (is_array($materials) && in_array($type, $materials)) 
            return true;
        else if ($type == $materials)
            return true;
        else return false;
    }
    
    public function hidden($materials = []) {
        return !$this->isShow($materials);
    }
    
    public function formatDate($timestamp = false) {
        if (!$timestamp)
            $timestamp = strtotime($this->date_created);
        if (!$timestamp)
            return "";
        $datetime1 = new \DateTime('@' . time());
        $datetime2 = new \DateTime('@' . $timestamp);
        $interval = $datetime1->diff($datetime2);

        $years = $interval->format('%y');
        $months = $interval->format('%m');
        $days = $interval->format('%a');
        $hours = $interval->format('%h');
        $minutes = $interval->format('%i');
        $seconds = $interval->format('%S');

        $elapsed = Yii::t("material", "Now");
        if ($seconds && $seconds != "00") {
            $elapsed = $seconds . " ";
            if ($seconds == 1)
                $elapsed .= Yii::t("material", 'Second');
            else if ($seconds > 1 && $seconds < 5)
                $elapsed .= Yii::t("material", '_Seconds');
            else $elapsed .= Yii::t("material", 'Seconds');
        }
        if ($minutes) {
            $elapsed = $minutes . " ";
            if ($minutes == 1)
                $elapsed .= Yii::t("material", 'Minute');
            else if ($minutes > 1 && $minutes < 5)
                $elapsed .= Yii::t("material", '_Minutes');
            else $elapsed .= Yii::t("material", 'Minutes');
        }
        if ($hours) {
            $elapsed = $hours . " ";
            if ($hours == 1)
                $elapsed .= Yii::t("material", 'Hour');
            else if ($hours > 1 && $hours < 5)
                $elapsed .= Yii::t("material", '_Hours');
            else $elapsed .= Yii::t("material", 'Hours');
        }
        if ($days) {
            $elapsed = $days . " ";
            if ($days == 1)
                $elapsed .= Yii::t("material", 'Day');
            else if ($days > 1 && $days < 5)
                $elapsed .= Yii::t("material", '_Days');
            else $elapsed .= Yii::t("material", 'Days');
        }
        if ($months) {
            $elapsed = $months . " ";
            if ($months == 1)
                $elapsed .= Yii::t("material", 'Month');
            else if ($months > 1 && $months < 5)
                $elapsed .= Yii::t("material", '_Months');
            else $elapsed .= Yii::t("material", 'Months');
        }
        if ($years) {
            $elapsed = $years . " ";
            if ($years == 1)
                $elapsed .= Yii::t("material", 'Year');
            else if ($years > 1 && $years < 5)
                $elapsed .= Yii::t("material", '_Years');
            else $elapsed .= Yii::t("material", 'Years');
        }

        return $elapsed;
    }
}
