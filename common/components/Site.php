<?php
namespace app\components;

use Yii;
use app\models\Language;
use app\modules\settings\models\SettingsMenu;
use app\modules\settings\models\SettingsFooter;
use app\modules\material\models\Material;

class Site {  
    private static $_instance = null;
    public static $menu = [];
    public static $footer = [];
    public static $seo = false;
    
    //Для кеширования
    const CACHE_KEY_PREFIX = 'Yii.Site.Settings.';
    public static $cachingDuration = 60;
    public static $cacheID = 'cache';
    public static $material = "home";
    public static $template = false;
    public static $js = false;
    
    public static $language = "ru";
    public static $languages = array();
    public static $path = [
        "language",
        "city",
        "type",
        "section",
        "subsection",
        "path"
    ];
    
    private static $clear_cash = true;

    // приватный конструктор ограничивает реализацию getInstance ()
    private function __construct() {}
    // ограничивает клонирование объекта
    protected function __clone() {}
    public function import() {}
    public function get() {}

    /**
     * @return Site
     */
    public static function getInstance() {
        $key = $cache = false;
        $component = self::$cacheID;
        $cache = Yii::$app->$component;
        $language = isset(Yii::$app->session["language"])?Yii::$app->session["language"]:"ru";
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
            //Меню сайта
            $key = self::CACHE_KEY_PREFIX . 'menu.'.$language;
            if (self::$cachingDuration > 0 && self::$cacheID !== false && $cache !== null) {
                if (($data = $cache->get($key)) !== false && !self::$clear_cash) {
                    self::$menu = unserialize($data);
                }
                else  {
                    self::$menu = SettingsMenu::getList($language);
                    if (isset($cache)) {
                        $cache->set($key, serialize(self::$menu), self::$cachingDuration);
                    }
                }
            }
            
            
            if (count(self::$menu)) {
                $seo = self::$menu[0];
                foreach (self::$menu as $menu){
                    if (in_array($menu->url, ["/".self::$material, "/material/".self::$material])) {
                        $seo = $menu;
                    }
                }
                self::getInstance()->setMeta($seo, __FILE__);
            }
            // Конец формирования меню
            
            // Футтер
            $key = self::CACHE_KEY_PREFIX . 'footer.'.$language;
            if (self::$cachingDuration > 0 && self::$cacheID !== false && $cache !== null) {
                if (($data = $cache->get($key)) !== false && !self::$clear_cash) {
                    self::$footer = unserialize($data);
                }
                else  {
                    self::$footer = SettingsFooter::getList($language);
                    if (isset($cache)) {
                        $cache->set($key, serialize(self::$footer), self::$cachingDuration);
                    }
                }
            }
            
            if (count(self::$footer)) {
                foreach (self::$footer as $footer){
                    if (in_array($footer->url, ["/".self::$material, "/info/".self::$material])) {
                        self::getInstance()->setMeta($footer, __FILE__);
                        break;
                    }
                }
                
            }
            // Конец формирования футера
            
            
            
            self::$languages = Language::getList();
            
            self::$template = Material::TEMPLATE_DEFAULT;
            
           
            
        }
        self::$_instance->setLanguage($language, true);
        return self::$_instance;
    }
    
    public function setLanguage($language = "ru", $change = false) {
        if (in_array($language, self::$languages) && $change) {
            Yii::$app->session["language"] = $language;
            Yii::$app->language = $language;
            self::$language = $language;
        }
        
    }
    
    public function getLanguage() {
        return self::$language;
    }
    
    public function getPath() {
        //Создать путь из переменной
        return self::$language."/".self::$path['type']."/".self::$path['city']."/".self::$path['section']."/".self::$path['subsection']."/".self::$path['path'];
    }
    
    public static function buildPath($language = "ru", $type = false, $city = false, $section = false, $subsection = false, $path = false) {
        $site = self::getInstance();
        if (self::$language != $language)
            $site->setLanguage($language);
        self::$path['language'] = self::$language;
        self::$path['city'] = $city;
        self::$path['type'] = $type;
        self::$path['section'] = $section;
        self::$path['subsection'] = $subsection;
        self::$path['path'] = $path;
        return $site->getPath();
    }
    
    public function setTemplate($templ = Material::TEMPLATE_DEFAULT) {
        self::$template = $templ;
    }
    
    public function getTemplate() {
        return isset(self::$template)?self::$template:Material::TEMPLATE_DEFAULT;
    }
    
    public function setJavaScript($js = false) {
        self::$js = $js;
    }
    
    public function getJavaScript() {
        return isset(self::$js)?self::$js:false;
    }
    
    public function getMenu() {
        return !empty(self::$menu)?self::$menu:false;
    }
    
    public function getFooter() {
        return !empty(self::$footer)?self::$footer:false;
    }
    
    public function getMeta() {
        $default = new \stdClass();
        $default->title = false;
        $default->description = false;
        $default->keywords = false;
        $default->site = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
        $default->image = null;
        $default->file = __FILE__;
        return !empty(self::$seo)?self::$seo:$default;
    }
    
    public function setMeta($item = [], $filename = __FILE__, $test = false) {
        if (!(self::$seo instanceof \stdClass))
            self::$seo = new \stdClass();
        
        self::$seo->title = !empty($item['seo_title'])?$item['seo_title']:false;
        self::$seo->description = !empty($item['seo_description'])?$item['seo_description']:false;
        self::$seo->keywords = !empty($item['seo_keywords'])?$item['seo_keywords']:false;
        self::$seo->site = !empty($item['seo_site'])?$item['seo_site']:"http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
        $image = (!empty($item['image']))?$item['image']:false;
        //Пробуем достать из материала
//        if (!$image && isset($item->text)) {
//            $matches = [];
//            preg_match_all('!http://.+\.(?:jpe?g|png|gif)!Ui' , $item->text , $matches);
//            if (isset($matches[0]) && isset($matches[0][0]))
//                $image = $matches[0][0];
//        }
        self::$seo->image = $image;
        self::$seo->file = $filename; //Используется для тестирования
    }
    
    public function setMetaKey($key, $value, $file = false) {
        self::$seo->$key = $value;
        if ($file)
            self::$seo->file = $file;
        //Yii::$app->debug->show(self::$seo);
    }
    
    public static function image_exisits($url) {
        stream_context_set_default(
            array (
                'http' => array (
                    'method' => 'HEAD',
                    'timeout' => 6
                )
            )
        );

        $headers = @get_headers($url);
        if (preg_match("/(200 OK)$/", $headers[0])) {
            return true;
        } else {
            return false;
        }
    }

}
