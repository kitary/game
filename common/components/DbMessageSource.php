<?php

namespace common\components;

use Yii;
use common\models\Language;
use common\models\TranslateMessage;

class DbMessageSource extends \yii\i18n\MessageSource {

    const CACHE_KEY_PREFIX = 'Yii.DbMessageSource.';

    private $_messages = [];
    public $cachingDuration = 30;
    public $cacheID = 'cache';

    static function handleMissingTranslation($event) {
        //Yii::$app->debug->show($event);
        if (in_array($event->category, array('YiiDebug.yii-debug-toolbar', 'yii-debug-toolbar'))) {
            return false;
        }
        TranslateMessage::missingTranslation($event->category, $event->message, $event->language);
        $event->translatedMessage = $event->message;
        return $event;
    }

    protected function loadMessages($category, $language) {
        $component = $this->cacheID;
        if ($this->cachingDuration > 0 && $this->cacheID !== false && ($cache = Yii::$app->$component) !== null) {
            $key = self::CACHE_KEY_PREFIX . '.messages.' . $category . '.' . $language;
            if (($data = $cache->get($key)) !== false) {
                return unserialize($data);
            }
        }

        $messages = $this->loadMessagesFromDb($category, $language);

        if (isset($cache)) {
            $cache->set($key, serialize($messages), $this->cachingDuration);
        }
        //Yii::$app->debug->show($cache);
        return $messages;
    }

    protected function translateMessage($category, $message, $language) {
        $key = $language . '.' . $category;
        if (!isset($this->_messages[$key]))
            $this->_messages[$key] = $this->loadMessages($category, $language);
        if (isset($this->_messages[$key][$message]['translation']) && $this->_messages[$key][$message]['translation'] !== '')
            return $this->_messages[$key][$message]['translation'];
        elseif ($this->hasEventHandlers(self::EVENT_MISSING_TRANSLATION) && (!isset($this->_messages[$key][$message]['status']) || $this->_messages[$key][$message]['status'] == 0)) {
            $event = new \yii\i18n\MissingTranslationEvent([
                'category' => $category,
                'message' => $message,
                'language' => $language,
            ]);
            $this->trigger(self::EVENT_MISSING_TRANSLATION, $event);
            if ($event->translatedMessage !== null) {
                return $this->_messages[$key][$message] = $event->translatedMessage;
            }
        }
        else return $message;
    }

    protected function loadMessagesFromDb($category, $language) {
        $id_language = Language::getLanguageId(strtolower($language));
        $translations = TranslateMessage::find()->select("message, translation, status")->where(["id_language" => $id_language, "category" => $category])->all();
        $messages = [];
        foreach ($translations as $translation) {
            $messages[$translation['message']]['translation'] = $translation['translation'];
            $messages[$translation['message']]['status'] = $translation['status'];
        }
        return $messages;
    }

}
