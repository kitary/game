<?php

namespace common\components;

class AuthAction extends \yii\authclient\AuthAction {
    
    public $page    = false;
    public $act     = false;

    protected function authSuccess($client) {
        if (!is_callable($this->successCallback)) {
            throw new InvalidConfigException('"' . get_class($this) . '::successCallback" should be a valid callback.');
        }
        $response = call_user_func($this->successCallback, $client);
        if ($response instanceof Response) {
            return $response;
        }
        return $this->redirectSuccess($response);
    }
    
    public function redirectSuccess($url = null) {
        //#send_comment_button
        return "<script>window.opener.closeModal('#send_comment_button'); close();</script>";
    }

}
