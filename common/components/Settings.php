<?php
namespace common\components;

use Yii;
use app\models\Mail;
use app\modules\settings\models\SettingsMenu;
use app\modules\settings\models\SettingsMail;
use app\modules\settings\models\SettingsMailTemplates;

class Settings {  
    private static $module_id = null;
    private static $module_path = null;
    private $settings = [];
    private static $_instance = null;
    public static $config = [];
    public static $mail = [];
    public static $mail_type = [];
    
    //Для кеширования
    const CACHE_KEY_PREFIX = 'Yii.Settings.';
    public static $cachingDuration = 30;
    public static $cacheID = 'cache';
    
    const MATERIAL      = "Material";
    const IMAGE         = "MaterialImages";
    const COMMENT       = "Comment";
    const SECTION       = "Section";
    const SUB_SECTION   = "SubSection";

    // приватный конструктор ограничивает реализацию getInstance ()
    private function __construct() {}
    // ограничивает клонирование объекта
    protected function __clone() {}
    public function import() {}
    public function get() {}

    /**
     * 
     * @return Settings
     */
    public static function getInstance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
            $component = self::$cacheID;
            //Загрузка основных настроек для сайта
            if (($key = self::CACHE_KEY_PREFIX . 'config') && !(self::$config = self::cashed($key))) {
                $data = \app\modules\settings\models\Settings::find()->one();
                if (($cache = Yii::$app->$component) !== null) {
                    $cache->set($key, serialize($data), self::$cachingDuration);
                }
                self::$config = $data;
            }       
            //Загрузка почтовых ящиков (их настройки)
            if (($key = self::CACHE_KEY_PREFIX . 'mail_box') && !(self::$mail = self::cashed($key))) {
                $data = [];
                $types = [Mail::MAIL_ADMIN, Mail::MAIL_DELIVERY, Mail::MAIL_FOR_USER, Mail::MAIL_INFO];
                foreach ($types as $type) {
                    $field = "id_".$type;
                    $data[$type] = SettingsMail::findOne(["id" => self::$config->$field]);
                }
                if (($cache = Yii::$app->$component) !== null) {
                    $cache->set($key, serialize($data), self::$cachingDuration);
                }
                self::$mail = $data;
            } 
            //Загрузка шаблонов почты
            if (($key = self::CACHE_KEY_PREFIX . 'mail_templates') && !(self::$mail_type = self::cashed($key))) {
                $data = [];
                if ($mail_types = SettingsMailTemplates::find()->all()) {
                    foreach ($mail_types as $mt) 
                        $data[$mt->type] = $mt->text;
                } 
                if (($cache = Yii::$app->$component) !== null) {
                    $cache->set($key, serialize($data), self::$cachingDuration);
                }
                self::$mail_type = $data;
            } 
        }
        return self::$_instance;
    }
    
    private static function cashed($key = false) {
        $component = self::$cacheID;
        if (self::$cachingDuration > 0 && $component !== false && ($cache = Yii::$app->$component) !== null && $key) {
            if (($data = $cache->get($key)) !== false) {
                //Yii::$app->debug->show(unserialize($data));
                return unserialize($data);
            }
        }
        return false;
    }
    
    public function visible($materials = []) {
        $type = $this->getModuleId();
        if (is_array($materials) && in_array($type, $materials)) 
            return true;
        else if ($type == $materials)
            return true;
        else return false;
    }
    
    public function hidden($materials) {
        return !$this->visible($materials);
    }
    
    public function getConfig($var) {
        return isset(self::$config->$var)?self::$config->$var:false;
    }
    
    public function getMail($type) {
        return isset(self::$mail[$type])?self::$mail[$type]:false;
    }
    
    public function getMailTemplate($type) {
        return isset(self::$mail[$type])?self::$mail[$type]->template:false;
    }
    
    public function getMailOfType($type) {
        return isset(self::$mail_type[$type])?self::$mail_type[$type]:false;
    }
    
    public function setModuleId($id) {
        self::$module_id = $id;
        $this->setModulePath($id);
    }
    
    public function getModuleId() {
        return self::$module_id;
    }
    
    public function setModulePath($id) {
        self::$module_path = 'app\\modules\\'.$id;
    }
    
    public function getModulePath() {
        return self::$module_path;
    }
    
    public function setModule($id) {
        $this->setModuleId($id);
        $this->setModulePath($id);
    }
    
    /**
     * 
     * @param type $material
     * @param type $type
     * @return type
     */
    public function getClassName($material = false, $type = self::MATERIAL) {
        if ($material)
            $this->setModule($material);
        return $this->getModulePath()."\models\\".$type;
    }

}
