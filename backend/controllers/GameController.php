<?php

namespace backend\controllers;

use Yii;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
//use yii\base\ViewContextInterface;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

use common\models\local\Language;
use backend\models\Game;

/**
 * NewsController implements the CRUD actions for News model.
 */
class GameController extends \yii\web\Controller {

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex() {
        $model = new Game();

        $dataProvider = $model->search(Yii::$app->request->get());

        return $this->render('index', [
            'searchModel' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'models' => $this->findModels($id),
                    'main' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $class_material = Settings::getInstance()->getModulePath() . "\models\Material";
        $class_material_images = Settings::getInstance()->getModulePath() . "\models\MaterialImages";
        $languages = Language::getList();
        $models = [];
        foreach ($languages as $id => $type) {
            $model = new $class_material();
            $model->id_language = $id;
            $model->scenario = "insert";
            $model->date_public = date("Y-m-d G:i:s", time());
            $models[$id] = $model;
        }

        if ($class_material::loadMultiple($models, Yii::$app->request->post())) {
            $id_main = 0;
            $gallery = false;
            $files = $_FILES;
            foreach ($models as $id_language => $model) {
                $model->sortFilesVariable($files, $model->id_language); //Модифицирует массив $_FILES согласно текущей модели
                $model->date_created = date("Y-m-d G:i:s", time());
                $model->fieldsClear();
                $model->_temp_gallery = UploadedFile::getInstances($model, '_temp_gallery');
                $model->id_main = $id_main;
                if (empty($model->short_text)) {
                    $model->short_text = strip_tags(htmlspecialchars_decode(substr($model->text, 0, 1000)));
                }
                $model->addNoFollow();
                if ($model->save()) {
                    $success = true;
                    if (!$id_main) {
                        $id_main = $model->id;
                        $class_material::updateAll(["id_main" => $id_main], ["id" => $model->id]);
                        //Загружаем галерею
                        if ($model->_temp_gallery) {
                            foreach ($model->_temp_gallery as $image) {
                                $class_material_images::addImage($id_main, $image);
                            }
                            $class_material::updateGalleryStatus($id_main, true);
                            $gallery = true;
                        }
                    }
                    if ($model->gallery)
                        $gallery = true;
                    Yii::$app->json->clearCache(Settings::getInstance()->getModuleId() . "/" . Yii::$app->formatter->asDate($model->date_created, 'php:Y-m-d'));
                }
            }
            if ($id_main) {
                if ($gallery)
                    return $this->redirect(['/admin/' . Settings::getInstance()->getModuleId() . '/images/update', 'id' => $id_main]);
                else
                    return $this->redirect(['view', 'id' => $id_main]);
            }
        }

        return $this->render('create', [
                    'models' => $models,
                    'languages' => $languages
        ]);
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $class_material = Settings::getInstance()->getModulePath() . "\models\Material";
        $class_material_images = Settings::getInstance()->getModulePath() . "\models\MaterialImages";
        $models = $this->findModels($id);
        $languages = Language::getList();
        foreach ($languages as $id_language => $type) {
            if (!isset($models[$id_language])) {
                $model = new $class_material();
                $model->id_language = $id_language;
                $model->date_created = date("Y-m-d G:i:s", time());
                $model->id_main = $id;
                $model->loadScenario("update");
                $models[$id_language] = $model;
            } else if (isset($models[$id_language]))
                $models[$id_language]->loadScenario("update", $id_language);
        }

        if ($class_material::loadMultiple($models, Yii::$app->request->post())) {
            $success = false;
            $gallery = false;
            $files = $_FILES;
            foreach ($models as $id_language => $model) {
                $model->sortFilesVariable($files, $model->id_language); //Модифицирует массив $_FILES согласно текущей модели
                $model->fieldsClear();
                $model->_temp_gallery = UploadedFile::getInstances($model, '_temp_gallery');
                if (empty($model->short_text)) {
                    $model->short_text = strip_tags(htmlspecialchars_decode(substr($model->text, 0, 1000)));
                }
                $model->addNoFollow();
                if ($model->validate()) {
                    if ($model->gallery)
                        $gallery = true;
                    if ($model->save()) {
                        $success = true;
                        Yii::$app->json->clearCache(Settings::getInstance()->getModuleId() . "/" . Yii::$app->formatter->asDate($model->date_created, 'php:Y-m-d'));
                        Yii::$app->json->clearCache(Settings::getInstance()->getModuleId() . "/" . $id . ".json");
                        //Загружаем галерею
                        if ($model->_temp_gallery && $model->id == $model->id_main) {
                            foreach ($model->_temp_gallery as $image) {
                                $class_material_images::addImage($model->id, $image);
                            }
                            $class_material::updateGalleryStatus($model->id, true);
                            $gallery = true;
                        }
                    }
                }
            }
            if (!$gallery)
                $class_material_images::deleteImagesFromObject($id);


            if ($success)
                return $this->redirect(['view', 'id' => $id]);
        }
        else {
            foreach ($models as $id_language => &$model) {
                //$model->text = str_replace("<p>", "", $model->text);
                //$model->text = str_replace("</p>", "\r", $model->text);
                $model->fieldsClear();
            }
        }

        return $this->render('update', [
                    'models' => $models,
                    'languages' => $languages,
                    'main' => $this->findModel($id),
        ]);
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $class_material_images = Settings::getInstance()->getModulePath() . "\models\MaterialImages";
        $models = $this->findModels($id);
        foreach ($models as $model) {
            $date = Yii::$app->formatter->asDate($model->date_created, 'php:Y-m-d');
            if ($model->delete()) {
                Yii::$app->json->clearCache(Settings::getInstance()->getModuleId() . "/" . $date);
                Yii::$app->json->clearCache(Settings::getInstance()->getModuleId() . "/" . $id . ".json");
                $class_material_images::deleteImagesFromObject($id);
            }
        }
        //return true;
        return $this->redirect("index");
    }

    //Удаление множества объектов, по ajax-запросу
    public function actionDeleteselected() {
        $class_material = Settings::getInstance()->getModulePath() . "\models\Material";
        $class_material_images = Settings::getInstance()->getModulePath() . "\models\MaterialImages";
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            if (!empty($post['items'])) {
                foreach ($post['items'] as $item) {
                    if ($model = $class_material::findOne($item)) {
                        $date = Yii::$app->formatter->asDate($model->date_created, 'php:Y-m-d');
                        if ($model->delete()) {
                            Yii::$app->json->clearCache(Settings::getInstance()->getModuleId() . "/" . $date);
                            Yii::$app->json->clearCache(Settings::getInstance()->getModuleId() . "/" . $item . ".json");
                            $class_material_images::deleteImagesFromObject($model->id);
                        }
                    }
                }
                return true;
            }
        }
    }

    //Удаление множества объектов, по ajax-запросу
    public function actionDelivery() {
        $class_material = Settings::getInstance()->getModulePath() . "\models\Material";
        $class_material_images = Settings::getInstance()->getModulePath() . "\models\MaterialImages";
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (Yii::$app->request->isAjax) {
            $post = Yii::$app->request->post();
            if (!empty($post['id'])) {
                if ($material = $class_material::find()->where(["id" => intval($post['id'])])->one()) {
                    //Yii::$app->debug->show($material);
                    $material->delivery_date = !empty($post['date']) ? $post['date'] : null;
                    //$material->setScenario("update");
                    if ($material->save())
                        return [
                            'result' => true,
                            "content" => Html::a(Html::tag('span', !empty($material->delivery_date) ? $material->delivery_date : Yii::t('material', 'Add to delivery') /* ,['class' => 'glyphicon glyphicon-play'] */ ), ['delivery'], [
                                'id' => 'delivery-button-' . $material->id,
                                'rel' => $material->delivery_date,
                                'onclick' => "addToDeliveryList(this, {$material->id}); return false;",
                            ])
                        ];
                }
            }
        }
        return ["result" => false];
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        $class_material = Settings::getInstance()->getModulePath() . "\models\Material";
        if (($model = $class_material::findOne($id)) !== null) {
            $model->fieldsClear();
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModels($id) {
        $class_material = Settings::getInstance()->getModulePath() . "\models\Material";
        $id_main = ($object = $class_material::findOne(["id" => $id])) ? $object->id_main : false;
        if ($items = $class_material::find()->where(["id_main" => $id_main])->orderBy("id_language ASC")->all()) {
            $models = [];
            foreach ($items as $item)
                $models[$item->id_language] = $item;
            return $models;
        } else {
            throw new NotFoundHttpException('The requested page does not existed.');
        }
    }

    public function actionSublist() {
        $class_subsection = Settings::getInstance()->getModulePath() . "\models\SubSection";
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $section_id = $parents[0];
                $out = $class_subsection::find()->select(['name' => 'name', 'id'])->where(["id_section" => $section_id])->orderBy("`name` ASC")->asArray()->all();

                //$out = ArrayHelper::map($items, 'id', 'name');
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionSectionlist() {
        $class_section = Settings::getInstance()->getModulePath() . "\models\Section";
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $language_id = $parents[0];
                $out = $class_section::find()->select(['name' => 'name', 'id'])->where(["id_language" => $language_id])->orderBy("`name` ASC")->asArray()->all();

                //$out = ArrayHelper::map($items, 'id', 'name');
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionCitieslist() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $country = $parents[0];
                $out = WorldCities::find()->select(['name' => 'name', 'id'])->where(["country" => $country])->orderBy("`name` ASC")->asArray()->all();
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionComment($id = false, $id_comment = false) {
        $class_comment = Settings::getInstance()->getModulePath() . "\models\Comment";
        $model = $this->findModel($id);

        if ($id_comment) { //Удаляем комментарий
            if ($class_comment::deleteAll(["id" => $id_comment]) && $model) {
//                $date = Yii::$app->formatter->asDate($model->date_created, 'php:Y-m-d');
//                Yii::$app->json->clearCache("news/".$date);
                $model->updateCommentsCount();
                Yii::$app->json->clearCache(Settings::getInstance()->getModuleId() . "/" . $id . ".json");
            }
            //$ids = News::getChieldComments($id, $id_comment);
            //print_r($ids); die();
        }


        $comment = new $class_comment();
        $comment->id_user = @Yii::$app->user->id;
        $sorted_comments = $class_comment::getSortedCommentsList($id);
        $commentProvider = new \yii\data\ArrayDataProvider([
            'key' => 'id', //or whatever you id actually is of these models.
            'allModels' => $class_comment::getCommentsList($sorted_comments),
            'pagination' => array('pageSize' => 20),
        ]); //$comment->search(Yii::$app->request->get(), $model->id);

        if ($comment->load(Yii::$app->request->post())) {
            $comment->id_material = $id;
            $comment->date_created = date("Y-m-d G:i:s", time());
            if ($comment->validate()) {
                if ($comment->save()) {
                    $model->updateCommentsCount();
                    Yii::$app->json->clearCache(Settings::getInstance()->getModuleId() . "/" . $id . ".json");
                    $comment->text = "";
                    $comment->id_user = @Yii::$app->user->id;
                }
            } else {
                print_r($comment->getErrors());
                die();
            }
        }

        $levels = $class_comment::getCommentsLevel($class_comment::getSortedCommentsList($id));

        return $this->render('comment', [
                    'model' => $model,
                    'commentProvider' => $commentProvider,
                    'commentModel' => $comment,
                    'levels' => $levels
        ]);
    }

    public function actionMenu($language = "ru") {
        $class_section = Settings::getInstance()->getModulePath() . "\models\Section";
        $languages = Language::getList();
        $menus = [];
        foreach ($languages as $id => $type) {
            $menu = $class_section::getItemsList($type);
            $menus[$id] = $menu;
        }

        return $this->render('menu', [
                    "menus" => $menus,
                    'language' => $language,
        ]);
    }

    public function actionDeleteMenuItem($id = false, $sub = false) {
        $class_section = Settings::getInstance()->getModulePath() . "\models\Section";
        $class_subsection = Settings::getInstance()->getModulePath() . "\models\SubSection";
        if ($sub) {
            $item = $class_subsection::find()->where(["id" => $id])->one();
            if ($item && $item->delete()) {
                Yii::$app->json->clearCache(Settings::getInstance()->getModuleId() . "/menu");
                return true;
            }
        } else {
            if ($item = $class_section::find()->where(["id" => $id])->one()) {
                $sub_items = $class_subsection::find()->where(['id_section' => $item->id])->all();
                foreach ($sub_items as $si) {
                    $si->delete();
                }
                if ($item->delete()) {
                    Yii::$app->json->clearCache(Settings::getInstance()->getModuleId() . "/menu");
                    return true;
                }
            }
        }
    }
    
    public function actionSetActiveSlide($type = false, $id_material = false, $id_image = false) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if ($type) {
            $class = Settings::getInstance()->getModulePath() . "\models\MaterialImages";
            $class::updateAll(['is_active' => 0], ["id_material" => $id_material]);
            $class::updateAll(['is_active' => 1], ["id" => $id_image,"id_material" => $id_material]);
            return ["result" => true, "active" => $id_image];
        }
    }
    
    public function actionDeleteSlide($id = false) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $class = Settings::getInstance()->getModulePath() . "\models\MaterialImages";
        $slide = $class::find()->where(["id" => $id])->one();
        if ($slide->delete())
            return ["result" => true];
    }

}
