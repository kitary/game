<?php

namespace backend\models;

use common\models\material\Material;

class Game extends Material {
    
    public static function tableName() {
        return "{{%games}}";
    }
    
}
