<?php

namespace app\modules\blogs\controllers;

use app\components\Settings;
use app\modules\material\components\Request;

class DefaultController extends \yii\web\Controller {
    
    public $request = null;

    public function init() {
        parent::init();
        $this->request = new Request(Settings::getInstance()->getModuleId());
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionSearch($id = false, $date = false, $language = "ru", $temp = false) {
        return $this->request->search($id, $date, $language, $temp);
    }

    //portblog/news/today?agglomeration=3
    public function actionToday($language = "ru", $temp = false) {
        return $this->request->today($language, $temp);
    }

    public function actionRss($language = "ru", $temp = false) {
        return $this->request->rss($language, $temp);
    }

    //portblog/news/view?id=1
    public function actionView($id = false, $language = "ru") {
        return $this->request->view($id, $language);
    } 
    
    public function actionMenu($language = "ru") {
        return $this->request->menu($language);
    }

}
