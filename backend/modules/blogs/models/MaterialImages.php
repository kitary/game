<?php

namespace app\modules\blogs\models;

use Yii;
use yii\web\UploadedFile;
use app\components\Settings;
use app\components\UploadImageBehavior;

use app\modules\blogs\models\Material;


class MaterialImages extends \app\modules\material\models\MaterialImages {
    
    public static function tableName() {
        return "{{%".Settings::getInstance()->getModuleId()."_images}}";
    }

}
