<?php

namespace backend\modules\settings\controllers;

use Yii;
use app\modules\settings\models\Settings;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\modules\settings\models\SettingsMenu;
use app\modules\settings\models\SettingsFooter;

use app\models\Language;

/**
 * DefaultController implements the CRUD actions for Settings model.
 */
class DefaultController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Settings models.
     * @return mixed
     */
    public function actionIndex($language = "ru") {
        $languages = Language::getList();
        $menus = [];
        foreach ($languages as $id => $type) {
            $menus[$id] = SettingsMenu::getList($type);
        }
        $footer = [];
        foreach ($languages as $id => $type) {
            $footer[$id] = SettingsFooter::getList($type);
        }
        
        return $this->render('index', [
            'menus' => $menus,
            'footer' => $footer,
            'language' => $language,
            'model' => Settings::find()->one(),
        ]);
    }

    /**
     * Updates an existing Settings model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['index', 'id' => $model->id]);
            } else
                Yii::$app->debug->show($model->getErrors());
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Settings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Settings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Settings::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
