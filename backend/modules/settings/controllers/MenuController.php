<?php

namespace app\modules\settings\controllers\admin;

use Yii;
use app\modules\settings\models\SettingsMenu;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\models\Language;

/**
 * MenuController implements the CRUD actions for SettingsMenu model.
 */
class MenuController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SettingsMenu models.
     * @return mixed
     */
    public function actionIndex($id_language = false) {       
        $dataProvider = new ActiveDataProvider([
            'query' => SettingsMenu::find()->where(['id_language' => $id_language]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'id_language' => $id_language,
        ]);
    }

    /**
     * Displays a single SettingsMenu model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id = false, $id_language = false) {
        return $this->render('view', [
            'id_language' => $id_language,
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SettingsMenu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_language = false) {
        $model = new SettingsMenu();
        $model->id_language = $id_language;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, "id_language" => $model->id_language]);
        } else {
            return $this->render('create', [
                'id_language' => $id_language,
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SettingsMenu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'id_language' => $model->id_language]);
        } else {
            return $this->render('update', [
                'id_language' => $model->id_language,
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SettingsMenu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $item = $this->findModel($id);
        $item->delete();

        return $this->redirect(['index', 'id_language' => $item->id_language]);
    }

    /**
     * Finds the SettingsMenu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SettingsMenu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = SettingsMenu::findOne(["id" => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionSavePosition() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (!empty($_POST['position']) && is_array($_POST['position'])) {
            foreach ($_POST['position'] as $index => $id) {
                SettingsMenu::updateAll(["position" => ($index+1)], ["id" => $id]);
            }
            return ["result" => true];
        }
        return ["result" => false];
    }

}
