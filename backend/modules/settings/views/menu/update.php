<?php

use yii\helpers\Html;

use app\models\Language;

/* @var $this yii\web\View */
/* @var $model app\modules\settings\models\SettingsMenu */

$this->title = Yii::t('settings', 'Update Menu Item: ') . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('settings', 'Settings'), 'url' => ['/admin/settings/index']];
$this->params['breadcrumbs'][] = ['label' => Language::getNameOfLanguage($id_language), 'url' => ['/admin/settings/index', 'language' => Language::getTypeOfLanguage($id_language)]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('settings', 'Main Menu'), 'url' => ['index', 'id_language' => $id_language]];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="settings-menu-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
