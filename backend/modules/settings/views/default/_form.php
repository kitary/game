<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\time\TimePicker;
use dosamigos\tinymce\TinyMce;

use app\models\Language;
use app\modules\settings\models\SettingsMail;

/* @var $this yii\web\View */
/* @var $model app\modules\settings\models\Settings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    echo $form->field($model, 'id_language', [
            'template' =>  "{label} ".Html::a('', ['/admin/language/create'], ['class' => 'glyphicon glyphicon-plus action-button'])." {input}" 
        ])->widget(Select2::classname(), [
        'options' => [
            'id'=>'language-id',
            'placeholder' => Yii::t('settings', 'Select a language..')
        ],
        'data' => ArrayHelper::map(Language::find()->select(['name', 'id'])->orderBy("`name` DESC")->all(), 'id', 'name')
    ]);
    ?>

    <?php
    echo $form->field($model, 'id_mail_delivery', [
            'template' =>  "{label} ".Html::a('', ['/admin/settings/mail/create'], ['class' => 'glyphicon glyphicon-plus action-button'])." {input}" 
        ])->widget(Select2::classname(), [
        'options' => [
            'id'=>'mail-delivery-id',
            'placeholder' => Yii::t('settings', 'Select a mail..')
        ],
        'data' => ArrayHelper::map(SettingsMail::find()->select(["CONCAT(email,' (',host,')') as email", 'id'])->orderBy("`email` DESC")->all(), 'id', 'email')
    ]);            
    ?>

    <?= $form->field($model, 'delivery_time')->widget(TimePicker::classname(), [
        'pluginOptions' => [
            'showSeconds' => false,
            'showMeridian' => false,
            'minuteStep' => 5,
        ]
    ]); ?>

    <?php
    echo $form->field($model, 'id_mail_for_user', [
            'template' =>  "{label} ".Html::a('', ['/admin/settings/mail/create'], ['class' => 'glyphicon glyphicon-plus action-button'])." {input}" 
        ])->widget(Select2::classname(), [
        'options' => [
            'id'=>'mail-for-user-id',
            'placeholder' => Yii::t('settings', 'Select a mail..')
        ],
        'data' => ArrayHelper::map(SettingsMail::find()->select(["CONCAT(email,' (',host,')') as email", 'id'])->orderBy("`email` DESC")->all(), 'id', 'email')
    ]);            
    ?>
    <?php
    echo $form->field($model, 'id_mail_info', [
            'template' =>  "{label} ".Html::a('', ['/admin/settings/mail/create'], ['class' => 'glyphicon glyphicon-plus action-button'])." {input}" 
        ])->widget(Select2::classname(), [
        'options' => [
            'id'=>'mail-info-id',
            'placeholder' => Yii::t('settings', 'Select a mail..')
        ],
        'data' => ArrayHelper::map(SettingsMail::find()->select(["CONCAT(email,' (',host,')') as email", 'id'])->orderBy("`email` DESC")->all(), 'id', 'email')
    ]);            
    ?>
    <?php
    echo $form->field($model, 'id_mail_admin', [
            'template' =>  "{label} ".Html::a('', ['/admin/settings/mail/create'], ['class' => 'glyphicon glyphicon-plus action-button'])." {input}" 
        ])->widget(Select2::classname(), [
        'options' => [
            'id'=>'mail-admin-id',
            'placeholder' => Yii::t('settings', 'Select a mail..')
        ],
        'data' => ArrayHelper::map(SettingsMail::find()->select(["CONCAT(email,' (',host,')') as email", 'id'])->orderBy("`email` DESC")->all(), 'id', 'email')
    ]);            
    ?>
    
    <?= 
    $form->field($model, "footer")->widget(TinyMce::className(), [
        'options' => ['rows' => 10],
        'language' => 'ru',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern imagetools"
            ],
            'toolbar1' => "insertfile undo redo | styleselect | bullist numlist outdent indent | link | print code preview fullscreen",
            'toolbar2' => "fontselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | forecolor backcolor",
            //'image_advtab' => true,
            "relative_urls" => false,
            "remove_script_host"=> false,
//            'file_browser_callback'=> new yii\web\JsExpression("function(field_name, url, type, win) {
//                if(type=='image') {
//                    $('#upload_form_model input').val('{$model->getShortName()}');
//                    $('#upload_form_object input').val({$model->id});
//                    $('#upload_form input[type=file]').click();
//                }
//            }"),
        ],
        
    ]); 
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
