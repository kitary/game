<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\tabs\TabsX;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model app\modules\settings\models\SettingsMailTemplates */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-mail-templates-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php 
    if ($model->isNewRecord)
        echo $form->field($model, 'type')->textInput(['maxlength' => true]);
    else echo $form->field($model, 'type')->textInput(['maxlength' => true, "disabled" => "disabled"]);
    ?>

    <?= 
    $form->field($model, "text")->widget(TinyMce::className(), [
        'options' => ['rows' => 10],
        'language' => 'ru',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern imagetools"
            ],
            'toolbar1' => "insertfile undo redo | styleselect | bullist numlist outdent indent | link image media | print code preview fullscreen",
            'toolbar2' => "fontselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | forecolor backcolor",
            'image_advtab' => true,
            "relative_urls" => true,
            "remove_script_host"=> true,
            'file_browser_callback'=> new yii\web\JsExpression("function(field_name, url, type, win) {
                if(type=='image') {
                    $('#upload_form_model input').val('Mail');
                    $('#upload_form_object input').val({$model->id});
                    $('#upload_form input[type=file]').click();
                }
            }"),
        ],
        
    ]); 
    ?>
    
    <h3>Допустимые переменные:</h3>
    <?php   
    $items = [
        [
            'label' => Yii::t('settings', 'User variables'),
            'content' => <<<STR
            <p><b>{LOGIN}</b> - Логин пользователя</p>
            <p><b>{USERNAME}</b> - Имя</p>
            <p><b>{RECOVERY_LINK}</b> - Ссылка для восстановления пароля</p>
STR
        ],
        [
            'label' => Yii::t('settings', 'Material variables'),
            'content' => <<<STR
            <p><b>{ID}</b> - Идентификатор материала</p>
            <p><b>{TITLE}</b> - Заголовок</p>
            <p><b>{IMAGE}</b> - Главное изображение</p>
            <p><b>{TEXT}</b> - Краткий текст</p>
            <p><b>{LINK}</b> - Ссылка на ресурс</p>
            <p><b>{INDEX}</b> - Текущий номер в списке</p>
            <p><b>{COMMENT}</b> - Комментарий</p>
STR
        ],
        [
            'label' => Yii::t('settings', 'Other variables'),
            'content' => <<<STR
            <p><b>{FILE}</b> - Текущее название шаблона</p>
STR
        ]
    ];
    
    echo TabsX::widget([
        'items' => $items,
        'position'=>TabsX::POS_ABOVE,
        'encodeLabels'=>false,
    ]);
    ?>

    <?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
