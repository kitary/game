<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model app\modules\settings\models\SettingsMail */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-mail-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'host')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'port')->textInput(['value' => 465]) ?>

    <?= $form->field($model, 'encryption')->textInput(['maxlength' => true, "value" => "tls"]) ?>

    <?= $form->field($model, 'file_transport')->dropDownList([0 => Yii::t('common', 'No'), 1 => Yii::t('common', 'Yes')]) ?>

    <?= $form->field($model, 'view_path')->textInput(['maxlength' => true, "value" => "@app/view"]) ?>
    
    <?= $form->field($model, 'description')->textInput() ?>
    
    <?= 
    $form->field($model, "template")->widget(TinyMce::className(), [
        'options' => ['rows' => 10],
        'language' => 'ru',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern imagetools"
            ],
            'toolbar1' => "insertfile undo redo | styleselect | bullist numlist outdent indent | link image media | print code preview fullscreen",
            'toolbar2' => "fontselect fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | forecolor backcolor",
            'image_advtab' => true,
            "relative_urls" => false,
            "remove_script_host"=> false,
            'file_browser_callback'=> new yii\web\JsExpression("function(field_name, url, type, win) {
                if(type=='image') {
                    $('#upload_form_model input').val('Mail');
                    $('#upload_form_object input').val({$model->id});
                    $('#upload_form input[type=file]').click();
                }
            }"),
        ],
        
    ]); 
    ?>
    
    <h3>Допустимые переменные:</h3>
    <p>
        <i>{CONTENT}</i> - Тело письма
    </p>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('common', 'Create') : Yii::t('common', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
