<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\settings\models\SettingsMail */

$this->title = 'Add Mail';
$this->params['breadcrumbs'][] = ['label' => Yii::t('settings', 'Settings'), 'url' => ['/admin/settings/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('settings', 'Mails'), 'url' => ['/admin/settings/mail/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-mail-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
