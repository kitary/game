<?php

namespace backend\modules\settings\models;

use Yii;
use common\models\Language;

/**
 * This is the model class for table "settings".
 *
 * @property integer $id
 * @property integer $id_language
 * @property integer $id_mail_delivery
 * @property integer $delivery_time
 * @property integer $id_mail_for_user
 * @property integer $id_mail_info
 * @property integer $id_mail_admin
 *
 * @property SettingsMail $mailDelivery
 * @property SettingsMail $mailForUser
 * @property SettingsMail $mailInfo
 * @property SettingsMail $mailAdmin
 */
class Settings extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_language', 'id_mail_delivery', 'id_mail_for_user', 'id_mail_info', 'id_mail_admin'], 'integer'],
            ['delivery_time', 'date', 'format' => "php:G:i"],
            [['footer'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_language' => Yii::t('settings', 'Language'),
            'id_mail_delivery' => Yii::t('settings', 'Mail Delivery'),
            'delivery_time' => Yii::t('settings', 'Delivery Time'),
            'id_mail_for_user' => Yii::t('settings', 'Mail For User'),
            'id_mail_info' => Yii::t('settings', 'Mail Info'),
            'id_mail_admin' => Yii::t('settings', 'Mail Admin'),
            'footer' => Yii::t('settings', 'Footer text'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(Language::className(), ['id' => 'id_language']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMailDelivery()
    {
        return $this->hasOne(SettingsMail::className(), ['id' => 'id_mail_delivery']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMailForUser()
    {
        return $this->hasOne(SettingsMail::className(), ['id' => 'id_mail_for_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMailInfo()
    {
        return $this->hasOne(SettingsMail::className(), ['id' => 'id_mail_info']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMailAdmin()
    {
        return $this->hasOne(SettingsMail::className(), ['id' => 'id_mail_admin']);
    }
    
    public static function getAll() {
        return Settings::find()->asArray()->one();
    }
    
    public static function getOne($type = false) {
        $settings = self::getAll();
        return isset($settings[$type])?$settings[$type]:false;
    }
}
